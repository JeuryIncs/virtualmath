<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Welcome extends CI_Controller {

function __construct(){
		parent::__construct();
		$this->load->helper('form');	
   		$this->load->helper('url');
		$this->load->Model('Model');
		$this->load->library('email');
		$this->load->library('session');
   			
	}

	public function index()
	{	
	if (isset($this->session->userdata['email']))
        {
          redirect('index', 'refresh');
       }
       
		$this->load->view('login');
	}

	function validateUser(){


		$data = $this->Model->validateUser();

		if(empty($data)){

			echo "The email you entered does not belong to any account";

		}else{

			echo "welcome";
			$dateLogin = array('email' => $data['email'], 'id' => $data['idstudent'], 'name' => $data['name']);
			$this->session->set_userdata($dateLogin); 
		}
	}



}
