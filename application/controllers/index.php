<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Index extends CI_Controller {

function __construct(){
		parent::__construct();
		$this->load->helper('form');	
   		$this->load->helper('url');
		$this->load->Model('Model');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->library('pagination');   			
	}

	public function index()
	{	
	if (empty($this->session->userdata['email']))
        {
          redirect('welcome', 'refresh');
       }

       $config['base_url'] = base_url()."index/index";
       $config['total_rows']= $this->Model->num_post();
       $config['per_page'] = 4;
       $config['num_links'] = 5;
       $config['full_tag_open'] = '<div class="pagination pagination-centered"><ul class="page_test">'; 
	   $config['full_tag_close'] = '</ul></div>';
	   $config['first_link'] = '&laquo; First';
	   $config['first_tag_open'] = '<li class="prev page">';
	   $config['first_tag_close'] = '</li>';

	   $config['last_link'] = 'Last &raquo;';
	   $config['last_tag_open'] = '<li class="next page">';
	   $config['last_tag_close'] = '</li>';
  
	   $config['next_link'] = 'Next &rarr;';
	   $config['next_tag_open'] = '<li class="next page">';
	   $config['next_tag_close'] = '</li>';

	   $config['prev_link'] = '&larr; Previous';
	   $config['prev_tag_open'] = '<li class="prev page">';
	   $config['prev_tag_close'] = '</li>';

	   $config['cur_tag_open'] = '<li class="active"><a href="">';
	   $config['cur_tag_close'] = '</a></li>';

	   $config['num_tag_open'] = '<li class="page">';
	   $config['num_tag_close'] = '</li>';
       $this->pagination->initialize($config);
       $date = array('post' => $this->Model->get_post($config['per_page']), 'pagination' => $this->pagination->create_links());       
       //	$fechasNow = $this->_elapsedTime($post['date']);
       	$comment = $this->Model->showComment();
		$this->load->view('header');
		$this->load->view('welcome',  $date/*, 'fechaactual' =>$fechasNow)*/);
		$this->load->view('footer');
	}	


	private function _elapsedTime($dateSave) 
	{
	if(empty($dateSave)) {
		  return "No hay fecha";
	}
   
	$intervalos = array("segundo", "minuto", "hora", "día", "semana", "mes", "año");
	$duraciones = array("60","60","24","7","4.35","12");
   
	$ahora = time();
	$Fecha_Unix = strtotime($dateSave);
	
	if(empty($Fecha_Unix)) {   
		  return "Fecha incorracta";
	}
	if($ahora > $Fecha_Unix) {   
		  $diferencia     =$ahora - $Fecha_Unix;
		  $tiempo         = "Hace";
	} else {
		  $diferencia     = $Fecha_Unix -$ahora;
		  $tiempo         = "Dentro de";
	}
	for($j = 0; $diferencia >= $duraciones[$j] && $j < count($duraciones)-1; $j++) {
	  $diferencia /= $duraciones[$j];
	}
   
	$diferencia = round($diferencia);
	
	if($diferencia != 1) {
		$intervalos[5].="e"; //MESES
		$intervalos[$j].= "s";
	}
   
    return "$tiempo $diferencia $intervalos[$j]";
	}


	function forum()
	{
	 if (empty($this->session->userdata['email']))
        {
          redirect('welcome', 'refresh');
       }
		$this->load->view('header');
		$this->load->view('forum');
		$this->load->view('footer');
	}	

	function workshop(){

	 if (empty($this->session->userdata['email']))
        {
          redirect('welcome', 'refresh');
       }
		$this->load->view('header');
		$this->load->view('workshop');
		$this->load->view('footer');
	}

	function test(){

	 if (empty($this->session->userdata['email']))
        {
          redirect('welcome', 'refresh');
       }
		$this->load->view('header');
		$this->load->view('test');
		$this->load->view('footer');
	}

	function virtualClass(){

	 if (empty($this->session->userdata['email']))
        {
          redirect('welcome', 'refresh');
       }
		$this->load->view('header');
		$this->load->view('virtualClass');
		$this->load->view('footer');
	}

	function Profile(){

	 if (empty($this->session->userdata['email']))
        {
          redirect('welcome', 'refresh');
       }
      	$profile = $this->Model->showProfile();

		$this->load->view('header');
		$this->load->view('profile', array('profile' =>$profile));
		$this->load->view('footer');
	}


	function publicPost(){

		$date = array(
		'title' => $this->input->post('txtTitle'),
		'bodyPost' => $this->input->post('bodyPost'),
		'date' => $this->_tiempoActual(),
		'user'=> $this->session->userdata['email']
		);

		$this->Model->savePost($date);
	}
   
    private function _tiempoActual(){
        date_default_timezone_set('Europe/Brussels');
        $fecha = date("d-m-Y G:i A");
        return  $fecha;
    }    

    function save_Comment($id_post){

    	$date = array(
    	'bodyComment' =>$this->input->post('bodyComment'),    	
		'id_user'=> $this->session->userdata['id'],
		'time' => $this->_tiempoActual(),
		'idpost' => $id_post
    		);

    	$this->Model->saveComment($date);

    }

    function changePicture(){	

		$idUser = $this->session->userdata['id'];
    	$target_path = "pictureProfile/";
		$target_path = $target_path . $idUser;
		move_uploaded_file($_FILES['photo']['tmp_name'], $target_path);
		$this->Model->updatePicture($target_path);
		redirect('index/profile');
									        
	}

	function chageDateProfile(){

		$date =	array(
		'name' => $this->input->post('name'),
		'phone' => $this->input->post('phone')
		 );

		$this->Model->chageDateProfile($date);


	}

	function confirmPost(){

		$date = array(
		'titlePost' => $this->input->post('txtTitle'),
		'bodyPost' => $this->input->post('bodyPost')					
		);

		$this->load->view('header');
		$this->load->view('confirmPost', array('post' => $date));
		$this->load->view('footer');

	}

	function calculateMath(){
		$date = array(
		'value' => $this->input->post('txtValue')						
		);

		$direction['direction'] = "http://api.wolframalpha.com/v2/query?appid=YYUWYX-TJ6RHHG5Y6&input=".$date['value']."&format=image,plaintext";
 		echo $this->load->view('returnValue',$direction,TRUE);
    
	}

	function showView($num_page){

    $value = $this->Model->show_Page($num_page);   
	$comment = $this->Model->showComment($num_page);

	$this->load->view('header');
	$this->load->view('view', array('value' => $value, 'comment' => $comment));
	$this->load->view('footer');

	

	}

	function logout(){

    $this->session->unset_userdata('email');
    $this->session->sess_destroy();
    redirect('welcome','index');

    }

}