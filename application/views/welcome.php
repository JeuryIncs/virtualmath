<div class="container">
	<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 ">
		<div class="bs-example">
			<table class="table table-bordered tblworkshop" id='table'>			
			    <thead>
			        <tr>
			            <td><div class="row text-center result "><h1>Post</h1></div></td>
			        </tr>
			    </thead>
			    <tbody>			
			        <tr>
			        	<td>
	       					<?php foreach ($post as $datos): ?>								
								
							    <a name='viewPost' href="<?php echo(base_url())?>index/showView/<?= $datos['id_post'] ?>">
							    <?php echo ($datos['title'])?></a>
							    <?= "Por: ".$datos['user'] ?>
							    <hr>
							<?php endforeach; ?>
							<?= $pagination ?>
			         	</td>
			        </tr>					    	        
			    </tbody>
			</table>
		</div>
    </div>
</div>