<DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>VirtualMath</title>
  <meta name="author" content="JeuryInc">    
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="keywords" content="virtualmath, math, mathematical, virtualclass"><!-- styles -->
  <script type="text/javascript" src='<?php echo(base_url())?>/js/jquery.js'></script>
  <script src="<?php echo(base_url())?>/js/jquery-1.3.1.min.js"></script>
  <script src="<?php echo(base_url())?>/js/jquery.easing.1.3.js"></script>
  <link href="<?php echo(base_url())?>/css/Desing.css" rel="stylesheet">
  <link href="<?php echo(base_url())?>/css/css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo(base_url())?>/css/css/style.css" rel="stylesheet">
  <script type="text/javascript" src='<?php echo(base_url())?>/js/functionJeiEs.js'></script>    
  <script type="text/javascript" src="<?php echo(base_url())?>/js/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
  <link href="<?php echo(base_url())?>/font-awesome-4.0.3/css/font-awesome.css" rel="stylesheet">
</head>
<body>
  <div class="navbar ">
    <div class="navbar-inner">
      <div class="container"> 
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span> <span class="icon-bar"></span> 
          <span class="icon-bar"></span> </a> <a class="brand photoWelcome" href="<?php echo(base_url())?>/index"><img src="<?php echo(base_url())?>img/not-found.png"></a>
            
          <ul class="nav nav-collapse pull-right">
            <li><a href="<?php echo(base_url())?>index/profile" class='oneTitle'><i class="fa fa-user "></i> Profile</a></li>
            <li><a href="<?php echo(base_url())?>index/forum" class='oneTitle'><i class="fa fa-comments-o "></i> Forum</a></li>
            <li><a href="<?php echo(base_url())?>index/test" class='oneTitle'><i class="fa fa-pencil-square-o "></i> Test</a></li>
            <li><a href="<?php echo(base_url())?>index/workshop" class='oneTitle'><i class="fa fa-superscript"></i>  Workshop</a></li>
            <li><a href="<?php echo(base_url())?>index/virtualClass" class='oneTitle'><i class="fa fa-video-camera "></i> Virtual Class</a></li>  
            <li><a href="<?php echo(base_url())?>index/logout" class='oneTitle'><i class="fa fa-cogs"></i> Log Out</a></li>
         </ul>
      </div>
    </div>
  </div>
