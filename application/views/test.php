<div class="container">				
		 <div class="row">
			 <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 ">
				<div class="bs-example">
		    <table class="table table-bordered tblTest" id='table'>
		      <thead>
		        <tr>
		        	<td><div class="row text-center result"><h1>Test</h1></div></td>
		        </tr>
		      </thead>
		      <tbody>
		        <tr>
		         <td class='tblResult'>
		     	 <div class="container skills">
				      <div class="row">
				        <div class="span3">
				          <div class="ps">
				          </div>
				        </div>
				        <div class="span5">						          
				          <div> &nbsp; <p class="pull-right">Functions</p> </div>
				          <div class="expand-bg"> <span class="expand ps2"> &nbsp; </span> </div>
				        </div>
				      </div>
				      <div class="row">
				        <div class="span3">
				          <div class="ai">
				          </div>
				        </div>
				        <div class="span5">
				           <div> &nbsp; <p class="pull-right">Limits</p> </div>
				           <div class="expand-bg"> <span class="expand ps2"> &nbsp; </span> </div>
				        </div>
				      </div>
				      <div class="row">
				        <div class="span3">
				          <div class="html">
				          </div>
				        </div>
				        <div class="span5">
				        	 <div> &nbsp; <p class="pull-right">Derived</p> </div>
				           <div class="expand-bg"> <span class="expand ps2"> &nbsp; </span> </div>
				        </div>
				      </div>
				      <div class="row">
				        <div class="span3">
				          <div class="css">
				          </div>
				        </div>
				        <div class="span5">
			        	 <div> &nbsp; <p class="pull-right">Implied Functions</p> </div>
			          	 <div class="expand-bg"> <span class="expand ps2"> &nbsp; </span> </div>
				        </div>
				      </div>
				    </div>
		         </td>
		        </tr>			        
		      </tbody>
		    </table>
		  </div>
        </div>
	   </div>
</div>