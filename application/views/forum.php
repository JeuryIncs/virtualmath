<div class="container">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 ">
		<div class="bs-example">
			<table class="table table-bordered tblworkshop" id='table'>			
			    <thead>
			        <tr>
			        	<td><div class="row text-center result "><h1>Post</h1></div></td>
			        </tr>
			    </thead>
			    <tbody>				
					<tr>
			        	<td><div class="row text-center result"><p>Toolbar</p></div>			        	
					        <div class="row">
							    <ul class="social-icons text-center">
							      	<li><p >$$ \infty $$</p></li>
							        <li><p class='approx'>$$ \approx $$</p></li>
							        <li><p>$$ \int_a^b $$</p></li>
							        <li><p>$$ \lim_{x \to{+}\infty}{} $$</p></li>	
							        <li><p>$$ \sqrt[n]{a} $$</p></li>
							        <li><p>$$ \infty $$</p></li>
							        <li><p>$$ \neq $$</p></li>
							        <li><p>$$ \Delta $$</p></li>
							        <li><p>$$ \delta $$</p></li>
							        <li><p>$$ x^n  $$</p></li>
							        <li><p>$$ x_n $$</p></li>
							        <li><p>$$ \pi  $$</p></li>
							        <li><p>$$ \leq $$</p></li>
							        <li><p>$$ \geq $$</p></li>
							        <li><p>$$ \beta $$</p></li>
							        <li><p>$$ \frac{df}{dx} $$</p></li>
							        <li><p>$$ \frac{{\partial f}}{{\partial x}} $$</p></li>
							        <li><p>$$ f^{\prime\prime} $$</p></li>
							        <li><p>$$ f^{\prime} $$</p></li>
						        </ul>
			    		    </div>
			    	    </td>						
			        </tr>
			        <tr>
			       	    <td>
			       			<?php

								echo form_open("index/confirmPost");

								$txtTitle = array(
								'name' =>  'txtTitle',
								'placeholder' => 'Enter the title about your post: ',
								'class' =>    'txtTitlePost'	
								);						

								$bodyPost = array(
							    'name' =>  'bodyComment',
								'class' => 'txtNombre',
								'id'  =>  'bodyPost',
	 					       	'rows'  => '20',
						        'cols'  => '50'
								);

								$btnNewPost = array(
								'name' =>  'btnNewPost',
								'value' => 'New Post',
								'type' => 'submit',
								'class' =>    'btn btn-primary pull-right newpost'	
								);	

								echo form_input	($txtTitle);
								echo form_textarea($bodyPost);
								echo form_input	($btnNewPost);
								echo form_close();
						    ?>
			       	    </td>
			        </tr>					    	        
			    </tbody>
			</table>
		</div>
    </div>
</div>