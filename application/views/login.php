<html>
	<head>
		<title>VirtualMath</title>
			<script type="text/javascript" src='js/jquery.js'></script>
			<script src="<?php echo(base_url())?>/js/jquery-1.3.1.min.js"></script>
		    <script src="<?php echo(base_url())?>/js/jquery.easing.1.3.js"></script>
			<script type="text/javascript" src='<?php echo(base_url())?>/js/functionJeiEs.js'></script>
			<link rel="stylesheet" type="text/css" href="<?php echo(base_url())?>/css/bootstrap.css">
			<link rel="stylesheet" type="text/css" href="<?php echo(base_url())?>/css/Desing.css"> 
		    <meta charset="utf-8">
			<meta name="author" content="JeuryInc"> 
			<meta name="keywords" content="virtualmath, math, mathematical, virtualclass">
	</head>
	<body>
		<div class="container">
			<div class="row text-center">
			    <div  class="animated bounceInUp">
			            <h2 class='title'>Welcome to </h2>
			            <h1 class='title2'>VirtualMath</h1>
			    </div>
			</div>
			    <hr class="colorgraph">
					<div class="row">
 				            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 ">
				                <div class="animated bounceInLeft text-center">			
									<div class="qitem text-center differential">
										<a href="#"><img src="images/2.jpg"  class="img-circle"/></a>
										<span class="caption"><h2>Log In</h2></span>
									</div>
									<div class="qitem text-center calculus">
										<a href="#"><img src="images/3.jpg" class="img-circle"/></a>
										<span class="caption"><h2>Log In</h2></span>
									</div>	
								</div>
							</div>
						<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 login">					
							<?php

								 $formLogin = array('class' => 'formLogin');
								 echo form_open('welcome/validateUser', $formLogin);

									$email = array(
										'name' =>  'email',
										'placeholder' => 'Enter your email: ',
										'class' =>    'form-control mail'	
										);

									$password = array(
										'name' =>        'pass',
										'placeholder' =>   'Enter your Password: ',
										'class' =>        'form-control pass'
										);

									$btnEntrar = array(
								    'name' => 'btnEntrar',
								    'value' => 'Log In',
								    'class' => 'btn btn-lg  btn-primary btn-block fa fa-save',
								    'type' => 'submit'
									);
									
									echo form_input($email);
									echo form_password($password);
									echo form_submit($btnEntrar);
								
								echo form_close();
							?>
						</div>
					</div>
		 		<a target='popup' href="#" ><p id='forgotPass' class="text-center">Forgot your password?</p></a>		
		</div>
		<a target='popup' href="http://www.facebook.com/JeuryAnt" ><h3 id='footer' >JeuryInc Dev'</h3></a>			
	</body>
</html>

