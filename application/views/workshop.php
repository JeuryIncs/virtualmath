<div class="container">
	<div class="row text-center">
		<?php
			 $formCalculate = array('class'=>'formWorshop');
			 echo form_open('index/calculateMath', $formCalculate);
					$txtCalculate = array(
						'name' =>  'txtValue',
				   		'id' => 'valueCalculate',
						'placeholder' => 'Enter what you want to calculate or know about: ',
						'class' =>    'form-control valueCalculate'	
					);										

					$btnCalculate = array(
					    'name' => 'Calculate',
					    'value' => 'Calculate',
					    'class' => 'btn btn-primary btnCalculate',
					    'type' => 'submit'
					);						
					echo form_input($txtCalculate);
					echo form_submit($btnCalculate);
					echo form_close();
		?>
	</div>	
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 ">
			<div class="bs-example">
			    <table class="table table-bordered " id='table'>
			        <thead>
			            <tr>
			        	    <td><div class="row text-center result"><h1>Result</h1></div></td>
			            </tr>
			        </thead>
			        <tbody>
			            <tr>
			                <td class='tblResult'><div class="row text-center result"><img id='logoInicio' src="http://clayton-usedcars.com/wp-content/themes/Theme-1015-Free/images/not-found.png"></div></td>
			            </tr>			        
			        </tbody>
			    </table>
		    </div>
		</div>
	</div>
</div>