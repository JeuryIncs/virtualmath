
$(document).ready(function() {



$(".newComment").click(function(){
	$(".newComment").hide();
	$(".comment").slideDown(500);
});
$(".btnCancel").click(function(){
	$(".newComment").show();
	$(".comment").slideUp(100);
});


$(".formWorshop").submit(function(){

		var txtValue = $(".valueCalculate").val();

		if(txtValue === ""){
			$(".valueCalculate").focus().after('<span class="error7"></span>');
			$(".error7").fadeOut(1500);
			return false;
		}

		var date = {
			txtValue: $(".valueCalculate").val()
		};
		
		$.post("index/calculateMath",date,function(value){
			$(".tblResult").html(value);		
		});

	return false;


});


$(".infinity").click(function(){
	var infinity = "$$%20infty$$";

	var valueBody = $("#bodyPost").val();
	$("#bodyPost").append(valueBody+""+infinity);
});



$(".formLogin").submit(function(){

var nombre = $(".mail").val();
var password = $(".pass").val();

if(nombre === ""){
	$(".mail").focus().after('<span class="error7"> ¿What is your email?</span>');
	$(".error7").fadeOut(1500);
	return false;
}
if(password === ""){
	$(".pass").focus().after('<span class="error7"> ¿What is your password?</span>');
	$(".error7").fadeOut(1500);
	return false;
}
    var datosRegistrar = {
	email: $('.mail').val(),
	pass: $('.pass').val()
	};

	$.post("index.php/Welcome/validateUser",datosRegistrar,function(respuesta){

		if(respuesta === 'welcome'){
			window.location.href = 'http://localhost/MathVirtual/index';
		}else{
			$(".btn-block").focus().after('<span class="error7">'+respuesta+'</span>');
			$(".error7").fadeOut(2000);

			}
	});

	return false;

});


	//Custom settings
	var style_in = 'easeOutBounce';
	var style_out = 'jswing';
	var speed_in = 1000;
	var speed_out = 300;	

	//Calculation for corners
	var neg = Math.round($('.qitem').width() / 2) * (-1);	
	var pos = neg * (-1);	
	var out = pos * 2;
	
	$('.qitem').each(function () {
	
		url = $(this).find('a').attr('href');
		img = $(this).find('img').attr('src');
		alt = $(this).find('img').attr('img');
		
		$('img', this).remove();
		$(this).append('<div class="topLeft"></div><div class="topRight"></div><div class="bottomLeft"></div><div class="bottomRight"></div>');
		$(this).children('div').css('background-image','url('+ img + ')');

		$(this).find('div.topLeft').css({top:0, left:0, width:pos , height:pos});	
		$(this).find('div.topRight').css({top:0, left:pos, width:pos , height:pos});	
		$(this).find('div.bottomLeft').css({bottom:0, left:0, width:pos , height:pos});	
		$(this).find('div.bottomRight').css({bottom:0, left:pos, width:pos , height:pos});	

	}).hover(function () {
	
		$(this).find('div.topLeft').stop(false, true).animate({top:neg, left:neg}, {duration:speed_out, easing:style_out});	
		$(this).find('div.topRight').stop(false, true).animate({top:neg, left:out}, {duration:speed_out, easing:style_out});	
		$(this).find('div.bottomLeft').stop(false, true).animate({bottom:neg, left:neg}, {duration:speed_out, easing:style_out});	
		$(this).find('div.bottomRight').stop(false, true).animate({bottom:neg, left:out}, {duration:speed_out, easing:style_out});	
				
	},
	
	function () {

		$(this).find('div.topLeft').stop(false, true).animate({top:0, left:0}, {duration:speed_in, easing:style_in});	
		$(this).find('div.topRight').stop(false, true).animate({top:0, left:pos}, {duration:speed_in, easing:style_in});	
		$(this).find('div.bottomLeft').stop(false, true).animate({bottom:0, left:0}, {duration:speed_in, easing:style_in});	
		$(this).find('div.bottomRight').stop(false, true).animate({bottom:0, left:pos}, {duration:speed_in, easing:style_in});	
	
	}).click (function () {
		window.location = $(this).find('a').attr('href');	
	});

	$(".differential").click(function(){
		$(".differential").slideUp(400);
		$(".calculus").slideUp(400);
		$(".login").slideDown(400);
	});

});
	